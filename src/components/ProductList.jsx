import React, { Component } from 'react';
import Product from './Product';
import Data from '../fakeData.js';

class ProductList extends Component {
    render() {
        const products=Data.sort((a,b)=>(b.votes-a.votes));
        const productComponents = Data.map((product)=>(
            
                <Product
                    key={'product-' + product.id}
                    id={product.id}
                    title={product.title}
                    description={product.description}
                    imageProduit={product.productImageUrl}
                    url={product.url}
                    votes={product.votes}
                    submittedAvatarUrl={product.submittedAvatarUrl}
                />
            
        ))
        return(
            <div className="ui unstackable items">
            {productComponents}
        </div>
        )
        
    }
}

export default ProductList;